import Vue from "vue";
import App from "./App.vue";
import router from "./router/";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import wysiwyg from "vue-wysiwyg";

// Import Bootstrap an BootstrapVue CSS files (order is important)
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import "vue-wysiwyg/dist/vueWysiwyg.css";

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);
Vue.use(wysiwyg, {}); // config is optional. more below

Vue.config.productionTip = false;

function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        // eslint
      "(?:^|; )" + name.replace(/([.$?*|{}()[\]\\/+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      if (!getCookie('sessionid')) {
        next({ name: 'admin' });
      } else {
        next();
      }
    } else {
      next();
    }
  })

new Vue({ router, render: (h) => h(App) }).$mount("#app");
